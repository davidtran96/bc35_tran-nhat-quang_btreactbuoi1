import React, { Component } from "react";
import Banner from "./Banner";
import Content from "./Content";
import Footer from "./Footer";
import Navigate from "./Navigate";

export default class BtReactBuoi1 extends Component {
  render() {
    return (
      <div>
        <Navigate />
        <Banner />
        <Content />
        <Footer />
      </div>
    );
  }
}
