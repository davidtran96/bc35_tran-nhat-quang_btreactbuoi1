import React, { Component, Fragment } from "react";

export default class Footer extends Component {
  render() {
    return (
      <Fragment>
        <footer className="py-5 bg-dark">
          <div className="container">
            <p className="m-0 text-center text-white">
              Copyright © Your Website 2022
            </p>
          </div>
        </footer>
      </Fragment>
    );
  }
}
